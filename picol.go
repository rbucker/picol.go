package picol

import (
	"errors"
	"fmt"
	"strings"
	"strconv"
)

var (
	PICOL_RETURN   = errors.New("RETURN")
	PICOL_BREAK    = errors.New("BREAK")
	PICOL_CONTINUE = errors.New("CONTINUE")
)

type Var string
type CmdFunc func(i *Interp, argv []string, privdata interface{}) (string, error)
type Cmd struct {
	fn       CmdFunc
	privdata interface{}
}
type CallFrame struct {
	vars   map[string]Var
	parent *CallFrame
	level	int
}
type Interp struct {
	CmdInsideQuote bool
	Quiet          bool
	level          int
	callframe      *CallFrame
	commands       map[string]Cmd

}

func InitInterp() *Interp {
	return &Interp{
		CmdInsideQuote: true,
		Quiet:          false,
		level:          0,
		callframe:      &CallFrame{vars: make(map[string]Var)},
		commands:       make(map[string]Cmd),
	}
}

func (i *Interp) CmdArgs(procname string) (string, error) {
	cmd, ok := i.commands[procname]
	if !ok {
		return "", fmt.Errorf("undefined procname %s", procname)
	}
	pd := cmd.privdata
	pda := pd.([]string)
	return pda[0], nil
}

func (i *Interp) CmdBody(procname string) (string, error) {
	cmd, ok := i.commands[procname]
	if !ok {
		return "", fmt.Errorf("undefined procname %s", procname)
	}
	pd := cmd.privdata
	pda := pd.([]string)
	return pda[1], nil
}

func (i *Interp) Locals() []string {
	ret := make([]string, 0)
	frame := i.callframe
	for k, _ := range frame.vars {
		ret = append(ret, k)
	}
	return ret
}

func (i *Interp) Globals() []string {
	ret := make([]string, 0)
	frame := i.callframe
	for ; frame.level>0; frame = frame.parent { }
	for k, _ := range frame.vars {
		ret = append(ret, k)
	}
	return ret
}

func (i *Interp) ProcStack() []string {
	ret := make([]string, 0)
	for frame := i.callframe; frame != nil; frame = frame.parent {
		if k, ok := frame.vars["__procname__"]; ok {
			ret = append(ret, string(k))
		}
	}
	return ret
}

func (i *Interp) Vars() []string {
	ret := make([]string, 0)
	for frame := i.callframe; frame != nil; frame = frame.parent {
		for k, _ := range frame.vars {
			ret = append(ret, k)
		}
	}
	return ret
}

func (i *Interp) Level() int {
	return i.callframe.level
}

func (i *Interp) Procs() []string {
	keys := make([]string, 0, len(i.commands))
	for k := range i.commands {
		keys = append(keys, k)
	}
	return keys
}

func (i *Interp) Scope() map[string]Var {
	ret := make(map[string]Var)
	for frame := i.callframe; frame != nil; frame = frame.parent {
		for k, v := range frame.vars {
			_, ok := ret[k]
			if !ok {
				ret[k] = v
			}
		}
	}
	return ret
}
var UPVAR ="upvar|"
var GLOBAL ="global|"
func (i *Interp) Var(name string) (Var, bool) {
	for frame := i.callframe; frame != nil; frame = frame.parent {
		v, ok := frame.vars[name]
		if ok {
			// check upvar and global
			if strings.HasPrefix(string(v), GLOBAL) {
				for frame = frame.parent; frame.parent != nil; frame = frame.parent {}
				v, ok := frame.vars[name]
				if ok {
					return v, ok
				}
			} else if strings.HasPrefix(string(v), UPVAR) {
				name = strings.TrimPrefix(string(v), UPVAR)
				for frame = frame.parent; frame != nil; frame = frame.parent {
					v, ok := frame.vars[name]
					if ok {
						return v, ok
					}
				}
			} else if strings.HasPrefix(string(v), "::") {
				name = string(v)
				for ; frame != nil; frame = frame.parent {
					v, ok := frame.vars[name]
					if ok {
						return v, ok
					}
				}
			}
			return v, ok
		}
	}
	return "", false
}
func (i *Interp) SetVar(name, val string) {
	if v, ok := i.callframe.vars[name]; ok {
		// check upvar and global
		if strings.HasPrefix(string(v), GLOBAL) {
			frame := i.callframe
			// locate the first callframe
			for frame = frame.parent; frame.parent != nil; frame = frame.parent {}
			_, ok := frame.vars[name]
			if ok {
				frame.vars[name] = Var(val)
				return
			}
		} else if strings.HasPrefix(string(v), UPVAR) {
			name = strings.TrimPrefix(string(v), UPVAR)
			// look down the callframe for amatch
			for frame := i.callframe.parent; frame != nil; frame = frame.parent {
				_, ok := frame.vars[name]
				if ok {
					frame.vars[name] = Var(val)
					return
				}
			}
		} else if strings.HasPrefix(string(v), "::") {
			name = string(v)
			frame := i.callframe
			// locate the first callframe
			for frame = frame.parent; frame.parent != nil; frame = frame.parent {}
			_, ok := frame.vars[name]
			if ok {
				frame.vars[name] = Var(val)
				return
			}
		}
	} else if strings.HasPrefix(name, "::") {
		frame := i.callframe
		// locate the first callframe
		for ; frame.parent != nil; frame = frame.parent {}
		frame.vars[name] = Var(val)
		return
	}
	i.callframe.vars[name] = Var(val)
}

func (i *Interp) UnsetVar(name string) {
	delete(i.callframe.vars, name)
}

func (i *Interp) Command(name string) *Cmd {
	v, ok := i.commands[name]
	if !ok {
		return nil
	}
	return &v
}

func (i *Interp) ReregisterCommand(newname, name string) error {
	if _, ok := i.commands[newname]; ok {
		return fmt.Errorf("command %s already exists", newname)
	}
	i.commands[newname] = i.commands[name]
	return nil
}

func (i *Interp) RegisterReplaceCommand(name string, fn CmdFunc, privdata interface{}) error {
	i.commands[name] = Cmd{fn, privdata}
	return nil
}

func (i *Interp) RegisterCommand(name string, fn CmdFunc, privdata interface{}) error {
	c := i.Command(name)
	if c != nil {
		return fmt.Errorf("Command '%s' already defined", name)
	}

	i.commands[name] = Cmd{fn, privdata}
	return nil
}

func (i *Interp) DeregisterCommand(name string) error {
	c := i.Command(name)
	if c == nil {
		return fmt.Errorf("Command '%s' is not defined", name)
	}

	delete(i.commands, name)
	return nil
}

/* EVAL! */
func (i *Interp) Eval(t string) (string, error) {
	p := InitParser(t)
	p.CmdInsideQuote = i.CmdInsideQuote
	var result string
	var err error

	argv := []string{}

	for {
		prevtype := p.Type
		// XXX
		t = p.GetToken()
		if p.Type == PT_EOF {
			break
		}

		switch p.Type {
		case PT_VAR:
			if p.current()=='(' {
				p.next()
				idx := p.GetToken()
				if strings.HasSuffix(idx, ")") {
					idx = idx[:len(idx)-1]
				} else {
					p.next()
				}
				if p.Type == PT_VAR {
					v, ok := i.Var(idx)
					if !ok {
						return "", fmt.Errorf("No such variable '%s'", idx)
					}
					idx = string(v)
				}
				newt := fmt.Sprintf("array $%s getval %s", t, idx)
				//fmt.Printf("NEWT: %s\n", newt)
				result, err = i.Eval(newt)
				if err != nil {
					return result, err
				} else {
					t = result
				}

			} else {
				v, ok := i.Var(t)
				if !ok {
					return "", fmt.Errorf("No such variable '%s'", t)
				}
				t = string(v)
			}
		case PT_CMD:
			result, err = i.Eval(t)
			if err != nil {
				return result, err
			} else {
				t = result
			}
		case PT_ESC:
			// XXX: escape handling missing!
		case PT_SEP:
			prevtype = p.Type
			continue
		}

		// We have a complete command + args. Call it!
		if p.Type == PT_EOL {
			prevtype = p.Type
			if len(argv) != 0 {
				if len(argv) == 1 {
					if _, err := strconv.Atoi(argv[0]); err == nil {
						return argv[0], err
					}
				}
				c := i.Command(argv[0])
				if c == nil {
					return "", fmt.Errorf("No such command '%s'", argv[0])
				}
				if c.fn != nil {
					result, err = c.fn(i, argv, c.privdata)
					if err != nil {
						return result, err
					}
				}
				// TODO for this to work we need to know whether this is the first in the list
				// also... interactive vs script might be reasonale
				if result != "" && !i.Quiet {
					fmt.Printf("%s\n", result)
				}
			}
			// Prepare for the next command
			argv = []string{}
			continue
		}

		// We have a new token, append to the previous or as new arg?
		if prevtype == PT_SEP || prevtype == PT_EOL {
			argv = append(argv, t)
		} else { // Interpolation
			argv[len(argv)-1] = strings.Join([]string{argv[len(argv)-1], t}, "")
		}
		prevtype = p.Type
	}
	return result, nil
}
