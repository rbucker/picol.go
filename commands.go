package picol

import (
	"fmt"
	"strconv"
	"strings"
	"math"
)

func ArityErr(i *Interp, name string, argv []string) error {
	return fmt.Errorf("Wrong number of args for %s %s", name, argv)
}


// the function name is kinda wrong. However, I've expanded this evaluation function
func CommandMath(i *Interp, argv []string, pd interface{}) (string, error) {
	var c int
	if len(argv) == 1 {
		switch {
		case argv[0] == "true":
			c = 1
		case argv[0] == "false":
			c = 0
		default:
			return "", fmt.Errorf("unrecognized operator or arity: %s", argv[0])
		}
		return fmt.Sprintf("%d", c), nil

	}
	if len(argv) == 2 {
		if argv[1]=="true" || argv[1]=="false" {
			switch {
			case argv[0] == "!":
				if argv[1]=="false" || argv[1]==""{
					c=1
				} else {
					c=0
				}
			default:
				return "", fmt.Errorf("unrecognized operator or arity: %s", argv[0])
			}
			return fmt.Sprintf("%d", c), nil
		}
		a, aok := strconv.Atoi(argv[1])
		if aok == nil {
			switch {
			case argv[0] == "!":
				if a==0 || argv[1]=="false" ||argv[1]==""{
					c=1
				} else {
					c=0
				}
			default:
				return "", fmt.Errorf("unrecognized operator or arity: %s", argv[0])
			}
			return fmt.Sprintf("%d", c), nil
		} else {
			return "", fmt.Errorf("unrecognized not an int or bool: %s", argv[1])
		}

	}
	if len(argv) != 3 {
		return "", ArityErr(i, argv[0], argv)
	}
	a, aok := strconv.Atoi(argv[1])
	b, bok := strconv.Atoi(argv[2])
	if aok == nil && bok == nil {
		switch {
		case argv[0] == "**":
			c = int(math.Pow(float64(a), float64(b)))
		case argv[0] == "<<":
			c = int(a) << uint(b)
		case argv[0] == ">>":
			c = int(a) >> uint(b)
		case argv[0] == "&^":
			c = a &^ b
		case argv[0] == "&":
			c = a & b
		case argv[0] == "|":
			c = a | b
		case argv[0] == "^":
			c = a ^ b
		case argv[0] == "%":
			c = a % b
		case argv[0] == "+":
			c = a + b
		case argv[0] == "-":
			c = a - b
		case argv[0] == "*":
			c = a * b
		case argv[0] == "/":
			c = a / b
		case argv[0] == ">" || argv[0] == "gt":
			if a > b {
				c = 1
			}
		case argv[0] == ">=" || argv[0] == "ge":
			if a >= b {
				c = 1
			}
		case argv[0] == "<" || argv[0] == "lt":
			if a < b {
				c = 1
			}
		case argv[0] == "<=" || argv[0] == "le":
			if a <= b {
				c = 1
			}
		case argv[0] == "==" || argv[0] == "eq":
			if a == b {
				c = 1
			}
		case argv[0] == "!=" || argv[0] == "ne":
			if a != b {
				c = 1
			}
		default: // FIXME I hate warnings
			c = 0
		}

	} else {
		a := argv[1]
		b := argv[2]
		switch {
		case argv[0] == ">" || argv[0] == "gt":
			if a > b {
				c = 1
			}
		case argv[0] == ">=" || argv[0] == "ge":
			if a >= b {
				c = 1
			}
		case argv[0] == "<" || argv[0] == "lt":
			if a < b {
				c = 1
			}
		case argv[0] == "<=" || argv[0] == "le":
			if a <= b {
				c = 1
			}
		case argv[0] == "==" || argv[0] == "eq":
			if a == b {
				c = 1
			}
		case argv[0] == "!=" || argv[0] == "ne":
			if a != b {
				c = 1
			}
		default: // FIXME I hate warnings
			c = 0
		}

	}
	return fmt.Sprintf("%d", c), nil
}

func CommandSet(i *Interp, argv []string, pd interface{}) (string, error) {
	if len(argv) != 3 && len(argv) != 2 {
		return "", ArityErr(i, argv[0], argv)
	}
	if len(argv)==2 {
		if x, ok := i.Var(argv[1]); ok {
			return string(x), nil
		} else {
			return "", fmt.Errorf("set() varname(%s) missing", argv[1])
		}
	}
	i.SetVar(argv[1], argv[2])
	return argv[2], nil
}

func CommandUnset(i *Interp, argv []string, pd interface{}) (string, error) {
	if len(argv) != 2 {
		return "", ArityErr(i, argv[0], argv)
	}
	i.UnsetVar(argv[1])
	return "", nil
}

func CommandIf(i *Interp, argv []string, pd interface{}) (string, error) {
	//if len(argv) != 3 && len(argv) != 5 {
		//return "", ArityErr(i, argv[0], argv)
	//}
	var result string
	var err error
	// argv[0] = IF
	// argv[1] is the IF-TEST
	// argv[2] is the IF-TRUE
	// argv[5] is "else" or "elseif"
	// argv[6] if 5 was ELSE then 6 is ELSE-TRUE, else, if 5 was ELSEIF then 6 is ELSE-IF-TEST
	// argv[7] if 5 eas ELSE then 7 is index past len; if 5 is ELSEIF then 7 is the ELSE-IF-TRUE
	j := 0

	for {
		if j >= len(argv) {
			return "", err
		}
		if argv[j] == "if" || argv[j] == "elseif" {
			j++
			if _, err = strconv.Atoi(argv[j]); err==nil {
				result = argv[j]
			} else {
				result, err = i.Eval(argv[j])
				if err != nil {
					return "", err
				}
			}
			j++
			if r, _ := strconv.Atoi(result); r != 0 {
				return i.Eval(argv[j])
			}
			j++
		}
		if j >= len(argv) {
			return "", err
		}
		if argv[j] == "else" {
			j++
			return i.Eval(argv[j])
		}
	}

	return result, nil
}

func CommandWhile(i *Interp, argv []string, pd interface{}) (string, error) {
	if len(argv) != 3 {
		return "", ArityErr(i, argv[0], argv)
	}

	for {
		var result string
		var err error
		if _, err = strconv.Atoi(argv[1]); err==nil {
			result = argv[1]
		} else {
			result, err = i.Eval(argv[1])
			if err != nil {
				return "", err
			}
		}
		if r, _ := strconv.Atoi(result); r != 0 {
			result, err := i.Eval(argv[2])
			switch err {
			case PICOL_CONTINUE, nil:
				//pass
			case PICOL_BREAK:
				return result, nil
			default:
				return result, err
			}
		} else {
			return result, nil
		}
	}
}

func CommandRetCodes(i *Interp, argv []string, pd interface{}) (string, error) {
	if len(argv) != 1 {
		return "", ArityErr(i, argv[0], argv)
	}
	switch argv[0] {
	case "break":
		return "", PICOL_BREAK
	case "continue":
		return "", PICOL_CONTINUE
	}
	return "", nil
}

func PushCallFrame(i *Interp) {
	i.callframe = &CallFrame{vars: make(map[string]Var), parent: i.callframe, level: i.callframe.level+1}
}

func PopCallFrame(i *Interp) {
	i.callframe = i.callframe.parent
}

func CommandCallProc(i *Interp, argv []string, pd interface{}) (string, error) {
	//fmt.Printf("proc argv: %v\n", strings.Join(argv, "  --  "))
	var x []string

	if pd, ok := pd.([]string); ok {
		x = pd
	} else {
		return "", nil
	}

	//i.callframe = &CallFrame{vars: make(map[string]Var), parent: i.callframe, level: i.callframe.level+1}
	PushCallFrame(i)
	i.SetVar("__procname__", argv[0])
	//defer func() { i.callframe = i.callframe.parent }() // remove the called proc callframe
	defer func() { PopCallFrame(i) }() // remove the called proc callframe

	//fmt.Printf("proc: %v\n", argv)
	if strings.HasPrefix(argv[0] , "::") {
		f := strings.Split( argv[0], NS_SEP)
		f = f[:len(f)-1]
		namespace := strings.Join(f, NS_SEP)
		i.SetVar(NS_CURRENT, namespace)
	}


	//minarity := 0
	arity := 0

	// p = param signature
        p := InitParser(strings.TrimSuffix(strings.TrimPrefix(x[0], "{"), "}"))
        for ;p.Type!=PT_EOF; {
                tok := p.GetToken()
                switch p.Type {
                case PT_STR, PT_ESC:
			arity++
			if tok == "args" {
				if arity >= len(argv) {
					i.SetVar(tok, "")
				} else {
					newval := strings.Join(argv[arity:], " ")
					l, r := "", ""
					for _, v := range argv[arity:] {
						if strings.Contains(v, " ") || v[0] == '"' {
							l, r = "{", "}"
							break
						}
					}
					i.SetVar(tok, l+newval+r)
				}
			} else {
				if arity >= len(argv) {
					if strings.Contains(tok, " ") {
						v := strings.SplitN(tok, " ", 2)
						i.SetVar(v[0], v[1])
					} else {
						return "", fmt.Errorf("Proc '%s' called with wrong arg num", argv[0])
					}
				} else {
					newval := argv[arity]
					if len(newval) == 0 {
						newval = ""
					} else if newval[0] == '"' || strings.Contains(newval, " ") {
						//newval = "{"+newval+"}"
						newval = newval
					}
					if strings.Contains(tok, " ") {
						v := strings.SplitN(tok, " ", 2)
						i.SetVar(v[0], newval)
					} else {
						i.SetVar(tok, newval)
					}
				}
			}
                default:
                }
        }


	//for _, arg := range strings.Split(x[0], " ") {
		//if len(arg) == 0 {
			//continue
		//}
		//arity++
		//if arg == "args" {
			//minarity = arity
			//// TODO instead of Join use CSV encoding
			//i.SetVar(arg, strings.Join(argv[arity:], " "))
		//} else {
			//i.SetVar(arg, argv[arity])
		//}
	//}

	//if (minarity == 0 && arity != len(argv)-1) || minarity>len(argv)-1 {
		//return "", fmt.Errorf("Proc '%s' called with wrong arg num", argv[0])
	//}

	body := x[1]
	result, err := i.Eval(body)
	if err == PICOL_RETURN {
		err = nil
	}
	return result, err
}

var NS_SEP = "::"
var NS_GLOBAL = "::"
var NS_CURRENT = "_current_ns_"

func getnamespace(i *Interp) string {
        var retval string
        ns, ok := i.Var(NS_CURRENT)
	if ok {
		if string(ns) == "" {
			retval = NS_GLOBAL
		} else {
			retval = string(ns)
		}
                if !strings.HasSuffix(retval, NS_SEP) {
                        retval = retval+NS_SEP
                }
	}
	if retval == NS_SEP {
		retval = ""
	}
        return retval
}

func current(i *Interp) string {
        var retval string
        namespace, ok := i.Var(NS_CURRENT)
        if ok {
                if string(namespace) == "" {
                        retval = NS_GLOBAL
                } else {
                        retval = string(namespace)
                }
        } else {
                retval = NS_GLOBAL
        }
        // return current namespace :: or ::name 
        return retval
}

func CommandProc(i *Interp, argv []string, pd interface{}) (string, error) {
	// register a user "proc" as a command. 
	// the procname may have a prefix '::' indicating a partial namespace path
	// check the var NS_CURRENT that is not the complete prefix of the procname then prepend it
	if len(argv) != 4 {
		return "", ArityErr(i, argv[0], argv)
	}
	namespace := getnamespace(i)
	newfunc := argv[1]
	body := argv[3]
	var restore string
	if namespace != "" {
		currentnamespace := current(i)
		if currentnamespace == NS_SEP {
			//restore = "unset "+NS_CURRENT
			namespace = currentnamespace + namespace
		} else {
			//restore = "set "+NS_CURRENT+" "+currentnamespace
			namespace = currentnamespace + NS_SEP
		}
		newcurrent := "set "+NS_CURRENT+" "+namespace
		body = newcurrent+"\n"+body+"\n"+restore
	} else if strings.HasPrefix(argv[1], NS_SEP) {
		x := strings.Split(newfunc, NS_SEP)
		newfunc = x[len(x)-1]
		namespace = strings.Join(x[:len(x)-1], NS_SEP)+NS_SEP
		newcurrent := "set "+NS_CURRENT+" "+namespace
		//restore = "unset "+NS_CURRENT
		body = newcurrent+"\n"+body+"\n"+restore
	}
	//fmt.Printf("registering %v in %v  - BODY: %v\n", newfunc, namespace, body)
	return "", i.RegisterCommand(namespace+newfunc, CommandCallProc, []string{argv[2], body})
}

func CommandReturn(i *Interp, argv []string, pd interface{}) (string, error) {
	if len(argv) != 1 && len(argv) != 2 {
		return "", ArityErr(i, argv[0], argv)
	}
	var r string
	if len(argv) == 2 {
		r = argv[1]
	}
	return r, PICOL_RETURN
}

func CommandError(i *Interp, argv []string, pd interface{}) (string, error) {
	if len(argv) != 1 && len(argv) != 2 {
		return "", ArityErr(i, argv[0], argv)
	}
	return "", fmt.Errorf(argv[1])
}

func (i *Interp) RegisterCoreCommands() {
	name := [...]string{"%", "+", "-", "*", "/", ">", ">=", "<", "<=", "==", "!=", "true", "false", "eq", "ne", "gt", "ge", "lt", "le", "!", "**", "|", "&", ">>", "<<", "^", "&^"}
	for _, n := range name {
		i.RegisterCommand(n, CommandMath, nil)
	}
	i.RegisterCommand("set", CommandSet, nil)
	i.RegisterCommand("unset", CommandUnset, nil)
	i.RegisterCommand("if", CommandIf, nil)
	i.RegisterCommand("while", CommandWhile, nil)
	i.RegisterCommand("break", CommandRetCodes, nil)
	i.RegisterCommand("continue", CommandRetCodes, nil)
	i.RegisterCommand("proc", CommandProc, nil)
	i.RegisterCommand("return", CommandReturn, nil)
	i.RegisterCommand("error", CommandError, nil)
}
